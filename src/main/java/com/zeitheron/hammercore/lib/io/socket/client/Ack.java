package com.zeitheron.hammercore.lib.io.socket.client;

/**
 * Acknowledgement.
 */
public interface Ack {

    public void call(Object... args);

}

