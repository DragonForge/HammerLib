/* Decompiled with CFR 0_123. */
package com.zeitheron.hammercore.lib.zlib.utils;

public interface ICommand
{
	public String name();
	
	public void execute();
}
